﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Utilities
{
    class Constants
    {
        public const string GameName = "Tintbullet";
        public const int VersionMajor = 1;
        public const int VersionMinor = 0;
        public const int VersionRevision = 0;

        // MAKE SURE TO SET THIS TO FALSE!!1!!1!1111
        public const bool DebugMode = true;

        public static string GetGameName()
        {
            return GameName + ", Version: " + VersionMajor + "." + VersionMinor + "." + VersionRevision;
        }

        public const string TextWelcome = "<strong>Welcome to {0}!</strong>";
        public const string TextNoOtherPlayers = "There are no other players online.";
        public const string Text1OtherPlayer = "There is 1 other player online.";
        public const string TextSomeOtherPlayers = "There are {0} other players online.";
    }
}
