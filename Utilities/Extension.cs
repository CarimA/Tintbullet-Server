﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Glaciate;
using System.Text.RegularExpressions;

namespace Server.Utilities
{
    public static class Extension
    {
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        public static Color HashColor(this string value)
        {            
            string x = Auth.CalculateMD5Hash(value);

            byte[] conv = System.Text.Encoding.ASCII.GetBytes(x);
            byte rS = conv[0];
            byte gS = conv[8];
            byte bS = conv[16];
            
            return Color.FromArgb(rS, gS, bS);
        }

        public static string StripHTML(this string value)
        {
            char[] array = new char[value.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < value.Length; i++)
            {
                char let = value[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public static string ParseHTMLish(this string value)
        {
            string toParse = value;
            bool flip = true;

            while (toParse.IndexOf("**") >= 0)
            {
                if (flip)
                    toParse = toParse.ReplaceFirst("**", "<strong>");
                else
                    toParse = toParse.ReplaceFirst("**", "</strong>");

                flip = !flip;
            }
            flip = true;

            while (toParse.IndexOf("__") >= 0)
            {
                if (flip)
                    toParse = toParse.ReplaceFirst("__", "<em>");
                else
                    toParse = toParse.ReplaceFirst("__", "</em>");

                flip = !flip;
            }
            flip = true;

            return toParse;
        }
        public static string Linkify(this string Value)
        {
            // _ replaced &#95;
            string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[_.a-z0-9-]+\.[a-z0-9\/_:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            Regex r = new Regex(regex, RegexOptions.IgnoreCase);
            return r.Replace(Value, "<a href=\"$1\" title=\"$1\" target=\"_blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
        }

    }
}
