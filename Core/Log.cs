﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server.Core
{
    public class Log
    {
        public class LogList
        {
            LogType Type { get; set; }
            string Message { get; set; }
            public LogList(LogType Type, string Message)
            {
                this.Type = Type;
                this.Message = Message;
            }
            public string ToString()
            {
                return "[" + this.Type + "] " + this.Message;
            }
        }

        private static LogType logType = LogType.Information;
        private static List<LogList> logSave = new List<LogList>();

        public enum LogType
        {
            Information = 0,
            Admin,
            Socket,
            Warning,
            Error
        }

        // I probably shouldn't be using Type as a parameter name. Oh well.
        public static void Note(LogType Type, string Message)
        {
            string msg = "[" + DateTime.Now + "] " + Message;
            // Add to console.
            if (Type >= logType)
            {
                switch (Type)
                {
                    case LogType.Information: Console.ForegroundColor = ConsoleColor.White; break;
                    case LogType.Admin: Console.ForegroundColor = ConsoleColor.Cyan; break;
                    case LogType.Socket: Console.ForegroundColor = ConsoleColor.Green; break;
                    case LogType.Warning: Console.ForegroundColor = ConsoleColor.Yellow; Beep(3); break;
                    case LogType.Error: Console.ForegroundColor = ConsoleColor.Red; Beep(5); break;
                }

                Console.WriteLine(msg);
            }

            // Add to log.
            logSave.Add(new LogList(Type, msg));
        }

        public static void Beep(int Times)
        {
            Thread t = new Thread(
                   () =>
                   {
                       // asynchronous please.
                       for (int i = 0; i < Times; i++)
                           Console.Beep(2000, 300);
                   }
               );
            t.Start();
        }

        public static void SaveLog()
        {
            string filepath = Application.StartupPath + @"\data\logs\" + DateTime.Today.ToShortDateString().Replace('/', '-') + ".txt";
            List<string> log = new List<string>();

            foreach (var a in logSave)
            {
                log.Add(a.ToString());
            }
            
            if (File.Exists(filepath))
            {
                // Append!
                File.AppendAllLines(filepath, log);
            }
            else
            {
                // New!
                File.WriteAllLines(filepath, log);
            }
        }
    }
}
