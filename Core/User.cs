﻿using Alchemy.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

using Server.Core;
using Server.Socket;
using Server.Utilities;

namespace Server.Core
{
    public class User
    {
        [JsonIgnore]
        public UserContext Context { get; set; }

        public bool isStaff { get; set; }

        public string Username { get; set; }
        public string Color { get; set; }

        public void Save()
        {
            string u = JsonConvert.SerializeObject(this);
            File.WriteAllText(Application.StartupPath + @"\data\accounts\" + Username + ".txt", u);
            Log.Note(Log.LogType.Information, "Saved \"" + Username + "\"");
        }

        public static User Load(string username)
        {
            if (File.Exists(Application.StartupPath + @"\data\accounts" + username + ".txt"))
            {
                string u = File.ReadAllText(Application.StartupPath + @"\data\accounts" + username + ".txt", Encoding.UTF8);
                Log.Note(Log.LogType.Information, "Loaded \"" + username + "\"");
                return (User)JsonConvert.DeserializeObject(u);
            }
            else
            {
                User a = new User() { Username = username, isStaff = false };
                a.SetColor();
                a.Save();
                Log.Note(Log.LogType.Information, "Created new account: \"" + username + "\"");
                return a;
            }
        }

        public static User GetUser(List<User> users, UserContext context)
        {
            foreach (var u in users)
            {
                if (u.Context == context)
                    return u;
            }
            return null;
        }

        private void SetColor()
        {
            System.Drawing.Color c = this.Username.HashColor();
            this.Color = "rgb(" + c.R + "," + c.G + "," + c.B + ");";
        }
    }
}
