﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Server.Core;
using Server.Socket;
using Server.Utilities;

using Alchemy.Classes;

namespace Server.Socket
{
    class Send
    {
        public static void Notification(string Username, string Message)
        {
            Response.Package("Notification", new Dictionary<string, dynamic>()
	        {
	            { "Message", Message }
            }).ToMember(Username);
        }

        public static void Notification(UserContext Context, string Message)
        {
            Response.Package("Notification", new Dictionary<string, dynamic>()
	        {
	            { "Message", Message }
            }).ToContext(Context);
        }

        public static void ValidateLogin(string Username)
        {
            Response.Package("ValidateLogin", null).ToMember(Username);

            Response.Package("Chat", new Dictionary<string, dynamic>()
                {
                    { "Message", string.Format(Constants.TextWelcome, Constants.GetGameName()) }                    
                }).ToMember(Username);

            string message = "";
            switch (Program.WebServer.GetOnlineUsers() - 1)
            {
                case 0: message = Constants.TextNoOtherPlayers; break;
                case 1: message = Constants.Text1OtherPlayer; break;
                default: message = string.Format(Constants.TextSomeOtherPlayers, Program.WebServer.GetOnlineUsers() - 1); break;
            }

            Response.Package("Chat", new Dictionary<string, dynamic>()
                {
                    { "Message", message }                 
                }).ToMember(Username);

            OnlineList();
        }

        public static void ChatGlobal(string Message)
        {
            Response.Package("Chat", new Dictionary<string, dynamic>()
                {
                    { "Message", Message }
                }).ToAll();
        }

        public static void ChatMember(string Username, string Message)
        {
            Response.Package("Chat", new Dictionary<string, dynamic>()
                {
                    { "Message", Message }
                }).ToMember(Username);
        }

        public static void OnlineList()
        {
            string html = "<ul><li>Online Users: " + Program.WebServer.GetOnlineUsers() + "</li>";
            List<User> users = Program.WebServer.OnlineUsers;
            users = users.OrderBy(x => x.Username).ToList<User>();

            foreach (User uStaff in users)
            {
                if (uStaff.isStaff)
                    html += "<li><p style='color: " + uStaff.Color + "; font-weight: 700; margin: 0;'>#" + uStaff.Username + "</p></li>";
            }

            foreach (User u in users)
            {
                if (!u.isStaff)
                    html += "<li><p style='color: " + u.Color + "; margin: 0;'>" + u.Username + "</p></li>";
            }
            html += "</ul>";

            Response.Package("OnlineList", new Dictionary<string, dynamic>()
                {
                    { "Data", html }
                }).ToAll();
        }
    }
}
