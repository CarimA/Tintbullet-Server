﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Alchemy;
using Alchemy.Classes;

using Server.Core;
using Server.Socket;
using Server.Utilities;

namespace Server.Socket
{
    public class Web
    {
        private WebSocketServer WebSocket { get; set; }
        public List<User> OnlineUsers { get; set; }
            
        public Web()
        {
            this.WebSocket = new WebSocketServer(5204, IPAddress.Any)
            {
                OnReceive = OnReceive,
                OnSend = OnSend,
                OnConnected = OnConnected,
                OnDisconnect = OnDisconnect,
                TimeOut = new TimeSpan(0, 5, 0)
            };
            this.OnlineUsers =new List<User>();
        }

        public void Start()
        {
            this.WebSocket.Start();
        }

        public void Stop()
        {
            this.WebSocket.Stop();
        }

        private void OnReceive(UserContext context)
        {
            Log.Note(Log.LogType.Socket, "Received Data From :" + context.ClientAddress);
            Log.Note(Log.LogType.Socket, context.DataFrame.ToString());
            Receive.Handle(context);
        }

        private void OnConnected(UserContext context)
        {
            Log.Note(Log.LogType.Socket, "Client Connection From : " + context.ClientAddress);
        }

        private void OnSend(UserContext context)
        {
            //Log.Note(Log.LogType.Socket, "Data Sent To : " + context.ClientAddress);
        }

        private void OnDisconnect(UserContext context)
        {
            Log.Note(Log.LogType.Socket, "Client Disconnected : " + context.ClientAddress);

            foreach (User u in OnlineUsers)
            {
                if (u.Context == context)
                {
                    if (u.isStaff)
                        Send.ChatGlobal("#" + u.Username + " has left Tintbullet.");
                    else
                        Send.ChatGlobal(u.Username + " has left Tintbullet.");

                    Log.Note(Log.LogType.Warning, "User \"" + u.Username + "\" logged out.");
                    OnlineUsers.Remove(u);
                    Send.OnlineList();
                    break;
                }
            }
            // later handle some logout declare.
        }

        public int GetOnlineUsers()
        {
            return this.OnlineUsers.Count();
        }

        public void AddUser(User Data)
        {
            this.OnlineUsers.Add(Data);
        }
    }
}
