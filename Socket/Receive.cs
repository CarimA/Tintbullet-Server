﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Server.Core;
using Server.Socket;
using Server.Utilities;
using Alchemy.Classes;
using Newtonsoft.Json;

namespace Server.Socket
{
    class Receive
    {
        public static void Handle(UserContext context)
        {
            try
            {
                var json = context.DataFrame.ToString();
                dynamic obj = JsonConvert.DeserializeObject(json);

                switch ((string)obj.Type)
                {
                    case "Login": handleLogin(context, obj.Data); break;
                    case "Chat": handleChat(context, obj.Data); break;
                }
            }
            catch (Exception e)
            {
                // Bad JSON! For shame.
                var r = new Response { Type = "error", Data = new { e.Message } };
                context.Send(JsonConvert.SerializeObject(r));
            }
        }

        private static void handleChat(UserContext context, dynamic data)
        {
            string message = ((string)data.Message).StripHTML().ParseHTMLish().Linkify().Trim();
            if (message == "")
                return;

            string name = User.GetUser(Program.WebServer.OnlineUsers, context).Username;
            if (User.GetUser(Program.WebServer.OnlineUsers, context).isStaff)
                name = "#" + name;

            //string color = String.Format("#{0:X6}", User.GetUser(Program.OnlineUsers, context).Username.GetHashCode());
            Send.ChatGlobal("<span>[" + DateTime.Now.ToShortTimeString().ToString() + "]</span>&nbsp;<p style='color: " + User.GetUser(Program.WebServer.OnlineUsers, context).Color + "; font-weight: 700;'>" + name + ":</p><p>&nbsp;" + message);

            Log.Note(Log.LogType.Information, "Chat: " + name + " - " + message);
        }

        private static void handleLogin(UserContext context, dynamic data)
        {
            string username = data.Username;
            string password = data.Password;

            if (!Constants.DebugMode)
            {
                switch (Glaciate.Auth.Login(username, password))
                {
                    case Glaciate.LoginResponse.BadResponse:
                        Send.Notification(context, "The server was unable to authenticate your login. Try again in a few minutes.");
                        Log.Note(Log.LogType.Warning, "Glaciate was potentially down at this time.");
                        break;
                    case Glaciate.LoginResponse.UsernameDoesNotExist:
                        Send.Notification(context, "This account does not exist. Sign up to Glaciate's Forums by clicking Register.");
                        break;
                    case Glaciate.LoginResponse.RequiresActivation:
                        Send.Notification(context, "This account requires activation. Check your sign-up email address (and its spam), look for an email from Glaciate and follow the instructions to activate your account.");
                        Log.Note(Log.LogType.Warning, "User \"" + username + "\" attempted to log in: Requires Activation.");
                        break;
                    case Glaciate.LoginResponse.IncorrectPassword:
                        Send.Notification(context, "The password is incorrect.");
                        Log.Note(Log.LogType.Warning, "User \"" + username + "\" attempted to log in: Incorrect Password.");
                        break;
                    case Glaciate.LoginResponse.IsBanned:
                        Send.Notification(context, "This account is banned from Glaciate.");
                        Log.Note(Log.LogType.Warning, "User \"" + username + "\" attempted to log in: Banned.");
                        break;

                    case Glaciate.LoginResponse.OK:
                        User u = User.Load(username);
                        u.isStaff = false;
                        u.Context = context;
                        u.Save();
                        Program.WebServer.AddUser(u);
                        Send.ValidateLogin(username);
                        Send.ChatGlobal(username + " has logged in to Tintbullet.");
                        Log.Note(Log.LogType.Information, "User \"" + username + "\" logged in.");
                        break;

                    case Glaciate.LoginResponse.OKIsStaff:
                        User uStaff = User.Load(username);
                        uStaff.isStaff = true;
                        uStaff.Context = context;
                        uStaff.Save();
                        Program.WebServer.AddUser(uStaff);
                        Send.ValidateLogin(username);
                        Log.Note(Log.LogType.Information, "Staff \"" + username + "\" logged in.");
                        break;
                }
            }
            else
            {
                User uStaff = User.Load(username);
                uStaff.isStaff = true;
                uStaff.Context = context;
                uStaff.Save();
                Program.WebServer.AddUser(uStaff);
                Send.Notification(username, "Debug Mode is on.");
                Send.ValidateLogin(username);
                Send.ChatGlobal("#" + username + " has logged in to Tintbullet.");
                Log.Note(Log.LogType.Warning, "User \"" + username + "\" logged in.");
            }
        }
    }
}
