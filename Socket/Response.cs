﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alchemy.Classes;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Server.Core;
using Server.Socket;
using Server.Utilities;

namespace Server.Socket
{
    public class Response
    {
        public string Type { get; set; }
        public dynamic Data { get; set; }

        public static Response Package(string Type, Dictionary<string, dynamic> Data)
        {
            Response response = new Response();
            response.Type = Type;
            response.Data = Data;

            return (response);
        }
    }

    public static class ResponseExtension
    {
        public static void ToAll(this Response Value)
        {
            foreach (var u in Program.WebServer.OnlineUsers)
            {
                u.Context.Send(JsonConvert.SerializeObject(Value));
            }
        }

        public static void ToMember(this Response Value, string Username)
        {
            foreach (var u in Program.WebServer.OnlineUsers)
            {
                if (u.Username == Username)
                    u.Context.Send(JsonConvert.SerializeObject(Value));
            }
        }

        public static void ToAllButMember(this Response Value, string Username)
        {
            foreach (var u in Program.WebServer.OnlineUsers)
            {
                if (u.Username != Username)
                    u.Context.Send(JsonConvert.SerializeObject(Value));
            }
        }

        public static void ToContext(this Response Value, UserContext Context)
        {
            Context.Send(JsonConvert.SerializeObject(Value));
        }
    }
}
