﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Glaciate;

using Server.Core;
using Server.Socket;
using Server.Utilities;

using System.Windows.Forms;
namespace Server
{
    class Program
    {
        public static Web WebServer;

        static void Main(string[] args)
        {
            //FormClosingEventHandler += new FormClosingEventHandler(OnClosing);

            WebServer = new Web();
            WebServer.Start();

            Console.Title = Constants.GetGameName();
            Log.Note(Log.LogType.Information, "Server Started. Running Alchemy Websockets.");
            Log.Note(Log.LogType.Information, "Game: " + Constants.GetGameName() + ", Debug Mode: " + Constants.DebugMode);
            Log.Note(Log.LogType.Information, "[Type \"exit\" and hit enter to stop the server.]");          

            // Accept commands on the console and keep it alive
            var command = string.Empty;
            while (command != "exit")
            {
                command = Console.ReadLine();
                //Socket.Send.ToAll(command);
            }

            OnClosing(null, null);
        }

        static void OnClosing(FormClosingEventArgs e, object sender)
        {
            WebServer.Stop();
            Log.Note(Log.LogType.Information, "Server stopped.");
            Log.SaveLog();
            Environment.Exit(0);
        }
    }
}